import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df

def calc_median(l):
    sl = sorted(l)
    if len(l) % 2 == 1:
        return sl[len(l)//2]
    else:
        return (sl[len(l)//2] + sl[(len/(l)//2) + 1])/2   

def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    df["Title"] = df["Name"].str.split().str.get(1)

    Mr = df[df["Title"] == "Mr."]
    Mrs = df[df["Title"] == "Mrs."]
    Miss = df[df["Title"] == "Miss."]

    Mr_na = sum(Mr["Age"].isna())
    Mrs_na = sum(Mrs["Age"].isna())
    Miss_na = sum(Miss["Age"].isna())

    x =  Mr[Mr["Age"].isna() == False]["Age"].median()
    y =  Mrs[Mrs["Age"].isna() == False]["Age"].median()
    z =  Miss[Miss["Age"].isna() == False]["Age"].median()
    return [("Mr.", Mr_na, x), ("Mrs.", Mrs_na, y), ("Miss.", Miss_na, z)]
